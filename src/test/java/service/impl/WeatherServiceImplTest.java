package service.impl;

import assembler.OpenWeatherAssembler;
import junit.framework.TestCase;
import model.Weather;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;
import service.WeatherService;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class WeatherServiceImplTest {

    @Autowired
    private WeatherService weatherService;

    @Test
    public void shouldReturnResponseFrom3rdPArtyWeatheService() {
        Weather response = weatherService.getCurrentWeather("Tallinn");

        assertThat(response).isNotNull();
    }
}
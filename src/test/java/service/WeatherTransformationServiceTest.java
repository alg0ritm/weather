package service;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Anton on 11/01/2017.
 */
public class WeatherTransformationServiceTest {

    private WeatherTransformationService weatherTransformationService;

    @Before
    public void setUp() {
        weatherTransformationService = new WeatherTransformationService();
    }

    @Test
    public void shouldReturnCorrectTempInCelsius() {
        assertThat(weatherTransformationService.celsiusDegreesOf(0)).isEqualTo(-273.15d);
    }
}

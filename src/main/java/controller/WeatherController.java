package controller;

import model.Weather;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import service.WeatherService;

@RequestMapping("/api")
@RestController
public class WeatherController {

    @Autowired
    private WeatherService weatherService;

    @RequestMapping(value = "/weather/{cityName}", method = RequestMethod.GET)
    public Weather getWeatherForCity(@PathVariable String cityName) {
        return weatherService.getCurrentWeather(cityName);
    }
}

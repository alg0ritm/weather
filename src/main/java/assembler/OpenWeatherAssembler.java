package assembler;

import model.Weather;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.WeatherTransformationService;
import transport.OpenWeatherDto;

/**
 * Created by Anton on 11/01/2017.
 */
@Service
public class OpenWeatherAssembler {

    @Autowired
    private WeatherTransformationService weatherTransformationService;

    public Weather assembleFromDtoToModel(OpenWeatherDto dto) {
        Weather weather = new Weather();
        weather.setCeliusDegrees(weatherTransformationService.celsiusDegreesOf(dto.getMain().getTemp()));

        return weather;
    }
}

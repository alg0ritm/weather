package transport;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Anton on 11/01/2017.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenWeatherDto {

    private Coord coord;
    private Main main;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Coord {
        private String lon;
        private String lat;

        public String getLon() {
            return lon;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Main {
        private double temp;

        public double getTemp() {
            return temp;
        }

        public void setTemp(double temp) {
            this.temp = temp;
        }
    }


    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }


}

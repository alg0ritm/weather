package model;

/**
 * Created by Anton on 11/01/2017.
 */
public class Weather {
    double celiusDegrees;

    public double getCeliusDegrees() {
        return celiusDegrees;
    }

    public void setCeliusDegrees(double celiusDegrees) {
        this.celiusDegrees = celiusDegrees;
    }
}

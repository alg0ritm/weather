package service.impl;

import assembler.OpenWeatherAssembler;
import model.Weather;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import service.WeatherService;
import transport.OpenWeatherDto;

/**
 * Created by Anton on 11/01/2017.
 */
@Service
public class WeatherServiceImpl implements WeatherService {

    public static final String SERVICE_URL = "http://api.openweathermap.org/data/2.5/weather?appid=bffb0421e9f31495036a37094d066da2";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private OpenWeatherAssembler assembler;

    @Override
    public Weather getCurrentWeather(String cityName) {
        OpenWeatherDto response = restTemplate.getForObject(SERVICE_URL + "&q=" + cityName, OpenWeatherDto.class);
        return assembler.assembleFromDtoToModel(response);
    }
}

package service;

import org.springframework.stereotype.Service;

/**
 * Created by Anton on 11/01/2017.
 */
@Service
public class WeatherTransformationService {

    public double celsiusDegreesOf(double temp) {
        return temp - 273.15;
    }
}

package service;

import model.Weather;

/**
 * Created by Anton on 11/01/2017.
 */
public interface WeatherService {

    Weather getCurrentWeather(String cityName);
}


